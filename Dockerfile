FROM fpco/stack-build:lts-12.13

ENV DEBIAN_FRONTEND=noninteractive

# Install required Ubuntu packages
RUN apt-get update \
 && apt-get install -y --no-install-recommends \
     apt-transport-https \
     apt-utils \
     build-essential \
     ca-certificates \
     curl \
     git \
     iputils-ping \
     iputils-tracepath \
     jq \
     libgmp-dev \
     libpython-dev \
     lsb-release \
     locales \
     make \
     mercurial \
     netbase \
     python-pip \
     python-setuptools \
     software-properties-common \
     unzip \
     xz-utils \
     zlib1g-dev \
 && apt-get clean  \
 && rm -rf /var/lib/apt/lists/*

# Install Docker Engine
ENV DOCKER_VERSION=18.06.1~ce~3-0~ubuntu
RUN curl -fssl https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
 && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
 && apt-get update \
 && apt-get install -y --no-install-recommends docker-ce=${DOCKER_VERSION} \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Install mkdocs
RUN pip install mkdocs==0.16.3 mkdocs-cinder \
 && mkdocs --version

# Install fpco/cache-s3
ENV CACHE_S3_VERSION=0.1.4
RUN curl -sSL https://github.com/fpco/cache-s3/releases/download/v${CACHE_S3_VERSION}/cache-s3-v${CACHE_S3_VERSION}-linux-x86_64.tar.gz | tar xz -C /usr/local/bin 'cache-s3'

# Install nix
ENV NIX_VERSION=2.1.3
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys B541D55301270E0BCF15CA5D8170B4726D7198DE

RUN mkdir -m 0755 /nix \
    && useradd --shell /bin/bash -u 30000 -o -c "Nix build user" nixbld \
    && useradd --shell /bin/bash -o -c "Nix build user 1" -u 30001 -G nixbld nixbld1

RUN curl -o nix-${NIX_VERSION}-x86_64-linux.tar.bz2 https://nixos.org/releases/nix/nix-${NIX_VERSION}/nix-${NIX_VERSION}-x86_64-linux.tar.bz2 \
    && curl -o nix-${NIX_VERSION}-x86_64-linux.tar.bz2.asc https://nixos.org/releases/nix/nix-${NIX_VERSION}/nix-${NIX_VERSION}-x86_64-linux.tar.bz2.asc \
    && gpg --verify nix-${NIX_VERSION}-x86_64-linux.tar.bz2.asc \
    && echo "3169d05aa713f6ffa774f001cae133557d3ad72e23d9b6f6ebbddd77b477304f nix-2.1.3-x86_64-linux.tar.bz2" | sha256sum -c \
    && tar xjf nix-${NIX_VERSION}-x86_64-linux.tar.bz2 \
    && USER=root sh nix-${NIX_VERSION}-x86_64-linux/install \
    && ln -s /nix/var/nix/profiles/default/etc/profile.d/nix.sh /etc/profile.d/ \
    && rm -r /nix-$NIX_VERSION-x86_64-linux* \
    && rm -r /var/cache/apt/*

ENV \
    ENV=/etc/profile \
    USER=root \
    PATH=/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/sbin:/usr/local/bin \
    NIX_PATH=/nix/var/nix/profiles/per-user/root/channels
